import pytest
import ROOT

@pytest.fixture(scope='module')
def root_file():
    """ A module fixture is used to open the ROOT file once for this entire
    module and then close it when we're done.
    """
    f = ROOT.TFile.Open('run/myOutputFile.root')
    yield f
    f.Close()

def test_file_structure(root_file):
    # check that h_njets_raw exists
    h_njets_raw = root_file.Get("h_njets_raw")
    assert h_njets_raw

def test_histogram_integral(root_file):
    # get h_mjj_kin histogram
    h_mjj_kin = root_file.Get("h_mjj_kin")
    # get total number of x bins
    n_bins_x = h_mjj_kin.GetNbinsX()
    # compute integral (i.e. sum bin counts)
    n_events = h_mjj_kin.Integral(1, n_bins_x + 1)
    # this should equal 712 events
    assert n_events == 712

def test_histogram_bins(root_file):
    # get h_njets_kin histogram
    h_njets_kin = root_file.Get("h_njets_kin")
    # get some bins' contents
    bin_lst = []
    for i in [1, 3, 5, 10, 20]:
        bin_lst.append(h_njets_kin.GetBinContent(i))

    # print(bin_lst)
    assert bin_lst == [42, 289, 145, 1, 0]
